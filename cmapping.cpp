#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cmapping.h"
#include "XPLMUtilities.h"
#include "PluginSetup.h"
#include <math.h>

cMapping::cMapping(int type, int peripheralIndex, char * variable, float threshold) :
    dataRef(NULL),
    cVariableName(NULL),
    iIndex(0),
    bForceDataRefUpdate(false),
    bPreviousValueValid(false)
{
    iMappingType = type;
    iPoKeysPeripheral = peripheralIndex;
    fThreshold = threshold;
    iComparisonOperation = 0;

    //XPLMDebugString("Initializing mapping...\n");

    if (variable != NULL)
    {
        int len = (int)strlen(variable);
        if (variable[0] == '!')
        {
            // If variable starts with !, force the dataRef update
            cVariableName = (char *)malloc(len);
            strcpy(cVariableName, variable + 1);
            //strcpy_s(cVariableName, len, variable + 1);
            bForceDataRefUpdate = true;
        } else
        {
            cVariableName = (char *)malloc(len + 1);
            strcpy(cVariableName, variable);
            //strcpy_s(cVariableName, len, variable);
        }

        XPLMDebugString("\n - Looking for a data reference ");
        XPLMDebugString(cVariableName);

        if (bForceDataRefUpdate)
        {
            XPLMDebugString(" (forced update) ");
        }

        // Find dataRef
        dataRef = XPLMFindDataRef(cVariableName);

        if (dataRef == NULL)
        {
            XPLMDebugString(": not found\n");
        } else
        {
            XPLMDebugString(": done\n");
        }
    }
}

cMapping::~cMapping()
{
    if (cVariableName != NULL) free(cVariableName);
}



char cMapping::GetBoolValue()
{
    int tmpi;
    float tmpf;

    CheckDataRef();
    if (dataRef == NULL) return 0;

    switch (refType)
    {
        case 0:
            return 0;
            break;

        case 1: // Integer
            return doComparisonF((float)XPLMGetDatai(dataRef)) ? 1 : 0;

        case 2: // Float
            return doComparisonF(XPLMGetDataf(dataRef)) ? 1 : 0;

        case 3: // Vector integer
            XPLMGetDatavi(dataRef, &tmpi, iIndex, 1);
            return doComparisonF((float)tmpi) ? 1 : 0;

        case 4: // Vector float
            XPLMGetDatavf(dataRef, &tmpf, iIndex, 1);
            return doComparisonF(tmpf) ? 1 : 0;
    }
    return 0;
}

void cMapping::SetValue(float value)
{
    //char sbuffer[200];

    int readValue;
    float readValuef;

    int newValue;
    float newValuef;

    CheckDataRef();

    switch (refType)
    {
        case 0:
            break;

        case 1: // Integer
            readValue = XPLMGetDatai(dataRef);
            newValue = (int)(fThreshold * value);
            if (readValue != newValue)
            {
                XPLMSetDatai(dataRef, newValue);
                //sprintf_s(sbuffer, 1000, "Setting variable %s to %d -> value: %d\n", cVariableName, (int)(fThreshold * value), readValue);
                //XPLMDebugString(sbuffer);
            }
            break;

        case 2: // Float
            readValuef = XPLMGetDataf(dataRef);
            newValuef = fThreshold * value;
            if (fabs(readValuef - newValuef) > 1e-3)
            {
                XPLMSetDataf(dataRef, newValuef);

                //sprintf_s(sbuffer, 1000, "Setting variable %s to %f -> value: %f\n", cVariableName, (fThreshold * value), readValuef);
                //XPLMDebugString(sbuffer);
            }
            break;

        case 3: // Vector integer
            XPLMGetDatavi(dataRef, &readValue, iIndex, 1);
            newValue = (int)(fThreshold * value);
            if (readValue != newValue)
            {
                XPLMSetDatavi(dataRef, &newValue, iIndex, 1);

                //sprintf_s(sbuffer, 1000, "Setting variable %s [%d] to %d -> value: %d\n", cVariableName, iIndex, newValue, readValue);
                //XPLMDebugString(sbuffer);
            }
            break;

        case 4: // Vector float
            XPLMGetDatavf(dataRef, &readValuef, iIndex, 1);
            newValuef = fThreshold * value;
            if (fabs(readValuef - newValuef) > 1e-3)
            {
                XPLMSetDatavf(dataRef, &newValuef, iIndex, 1);

                //sprintf_s(sbuffer, 1000, "Setting variable %s [%d] to %f -> value: %f\n", cVariableName, iIndex, newValuef, readValuef);
                //XPLMDebugString(sbuffer);
            }
            break;
    }
}

float cMapping::GetFloatValue()
{
    float tmpf;
    int tmpi;

    CheckDataRef();
    if (dataRef == NULL) return 0;

    switch (refType)
    {
        case 0:
            return 0;
            break;

        case 1: // Integer
            return (float)XPLMGetDatai(dataRef) * fThreshold;
            break;

        case 2: // Float
            return XPLMGetDataf(dataRef) * fThreshold;
            break;

        case 3: // Vector integer
            XPLMGetDatavi(dataRef, &tmpi, iIndex, 1);
            return tmpi * fThreshold;

        case 4: // Vector float
            XPLMGetDatavf(dataRef, &tmpf, iIndex, 1);
            return tmpf * fThreshold;
    }
    return 0;
}

void cMapping::DefineNumberFormat(int places, int decimals, int zeropad)
{
    iplaces = places;
    idecimals = decimals;
    izeropad = zeropad;
}

void cMapping::DefineBufferOffset(int offset)
{
    iBufferOffset = offset;
}

void cMapping::ProduceString(char *dest)
{
    float value = GetFloatValue();
    float fact = 1;
    int minus = 0;


    if (value < 0)
    {
        value *= -1;
        minus = 1;
    }

    // First the integer part
    int p;
    memset(dest + iBufferOffset, ' ', iplaces);

    for (p = 0; p < iplaces; p++)
    {
        unsigned int decimal = (int)(value/fact);

        if (p < izeropad)
        {
            dest[iBufferOffset + iplaces - 1 - p] = '0';
        }

        if (decimal != 0)
        {
            dest[iBufferOffset + iplaces - 1 - p] = '0' + ((int)(value / fact) % 10);
        } else if (decimal == 0 && p >= izeropad)
        {
            dest[iBufferOffset + iplaces - 1 - p] = ' ';
            p++;
            break;
        }
        fact *= 10;
    }
    if (minus)
    {
        dest[iBufferOffset + iplaces - p] = '-';
    }

    if (idecimals > 0)
    {
        dest[iBufferOffset + iplaces] = '.';

        fact = 10;
        for (int d = 0; d < idecimals; d++)
        {
            dest[iBufferOffset + iplaces + 1 + d] = '0' + ((int)(value * fact) % 10);
            fact *= 10;
        }
    }
}

void cMapping::CheckDataRef()
{
    if (!bForceDataRefUpdate) return;

    if (dataRef == NULL)
    {
        //XPLMDebugString("\n - Looking for a data reference ");
        //XPLMDebugString(cVariableName);

        dataRef = XPLMFindDataRef(cVariableName);

        if (dataRef != NULL)
        {
            XPLMDebugString("\nFound dataRef for ");
            XPLMDebugString(cVariableName);
            bForceDataRefUpdate = false;
        }
    }
}

bool cMapping::doComparisonF(float value)
{
    switch (iComparisonOperation)
    {
        case 0:
            return (value > fThreshold);
        case 1:
            return (value == fThreshold);
        case 2:
            return (value < fThreshold);
        case 10:
            return (value >= fThreshold);
        case 11:
            return (value != fThreshold);
        case 12:
            return (value <= fThreshold);
    }
    return (value > fThreshold);
}


int parseVarType(char * linePart)
{
    int refVarType = 0;

    if (strcmp(linePart, "vi") == 0)
    {
        refVarType = 3;
    } else if (strcmp(linePart, "vf") == 0)
    {
        refVarType = 4;
    } else if (strcmp(linePart, "i") == 0)
    {
        refVarType = 1;
    } else if (strcmp(linePart, "f") == 0)
    {
        refVarType = 2;
    } else
    {
        refVarType = 0;
    }

    printf("\n - Type: %d", refVarType);

    return refVarType;
}

float parseOptionalValueF(char * linePart, float defaultValue)
{
    // Variable index
    float optVarValue = defaultValue;
    try
    {
        optVarValue = (float)atof(linePart);
    }
    catch (int)
    {
        //
    }
    return optVarValue;
}

int parseOptionalValueI(char * linePart, int defaultValue)
{
    // Variable index
    int optVarValue = defaultValue;
    try
    {
        optVarValue = atoi(linePart);
    }
    catch (int)
    {
        //
    }
    return optVarValue;
}


int parseVarIndex(char * linePart)
{
    // Variable index
    int refVarIndex = 0;
    try
    {
        refVarIndex = atoi(linePart);
        printf("\n - Index: %d", refVarIndex);
    }
    catch (int)
    {
        XPLMDebugString("Error parsing the index!\n");
    }
    return refVarIndex;
}

float parseVarThreshold(char * linePart)
{
    //XPLMDebugString("Parsing threshold... ");

    // Threshold/multiplier
    float refVarThreshold = 0;
    try
    {
        refVarThreshold = (float)atof(linePart);
        printf("\n - Threshold: %f", refVarThreshold);
    }
    catch (int)
    {
        XPLMDebugString("Error parsing the threshold!\n");
    }
    return refVarThreshold;
}

int parseComparisonOp(char * linePart)
{
    // 0 - GT, 1 - EQ, 2 - LT, 10 - GTE, 11 - NEQ, 12 - LTE

    if (strcmp("GTE", linePart) == 0)
        return 10;
    else if (strcmp("LTE", linePart) == 0)
        return 12;
    else if (strcmp("NEQ", linePart) == 0)
        return 11;
    else if (strcmp("GT", linePart) == 0)
        return 0;
    else if (strcmp("EQ", linePart) == 0)
        return 1;
    else if (strcmp("LT", linePart) == 0)
        return 2;

    return 0;
}
