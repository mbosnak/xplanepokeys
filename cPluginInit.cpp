#include <stdio.h>
#include <string.h>
#include "cpkdeviceinterface.h"
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "cPluginInit.h"

cPKDeviceInterface devices[MAXDEVICES];

#ifndef PluginTest
void DrawStatus(XPLMWindowID inWindowID)
{
    int		left, top, right, bottom;
    float	color[] = { 1.0, 1.0, 1.0 }; 	/* RGB White */

    /* First we get the location of the window passed in to us. */
    XPLMGetWindowGeometry(inWindowID, &left, &top, &right, &bottom);

    /* We now use an XPLMGraphics routine to draw a translucent dark
     * rectangle that is our window's shape. */
    XPLMDrawTranslucentDarkBox(left, top, right, bottom);


    XPLMDrawString(color, left + 5, top - 20, "PoKeys status:", NULL, xplmFont_Basic);

    char devStat[100];
    int devInd = 0;
    for (int i = 0; i < MAXDEVICES; i++)
    {
        if (devices[i].device != NULL)
        {
            sprintf_s(devStat, 100, "%s [%d]: Connected", devices[i].device->DeviceData.DeviceName, devices[i].device->DeviceData.SerialNumber);

            XPLMDrawString(color, left + 5, top - (devInd + 2) * 20 , devStat, NULL, xplmFont_Basic);

            devInd++;
        }
    }

    XPLMDrawString(color, left + 5, top - (devInd + 2) * 20 , "Click to hide status", NULL, xplmFont_Basic);
}
#endif

char * ReadFileContents(FILE * configFile)
{
    char *buf;
    int len;

    // Read complete file...
    fseek(configFile,0,SEEK_END); //go to end
    len=ftell(configFile); //get position at end (length)
    fseek(configFile,0,SEEK_SET); //go to beg.

    buf=(char *)malloc(len+1); //malloc buffer
    fread(buf,len,1,configFile); //read into buffer
    fclose(configFile);
    buf[len] = 0;

    return buf;
}

void StartRefresh()
{
    printf("\nStarting threads... \n");
    for (int i = 0; i < MAXDEVICES; i++)
    {
        if (devices[i].device == NULL) continue;

        devices[i].UpdateConfig();
        devices[i].RefreshOnce(1);
    }
}

void RefreshOnce()
{
    for (int i = 0; i < MAXDEVICES; i++)
    {
        if (devices[i].device == NULL) continue;
        devices[i].RefreshOnce(0);
    }
}

void InitAll()
{
    for (int i = 0; i < MAXDEVICES; i++)
    {
        devices[i].device = NULL;
    }
}

void DisconnectAll()
{
    for (int i = 0; i < MAXDEVICES; i++)
    {
        if (devices[i].device == NULL) continue;
        devices[i].Shutdown();
        PK_DisconnectDevice(devices[i].device);
        devices[i].device = NULL;
    }
}

int LoadConfiguration()
{
    FILE *	gConfigFile;
    char	configPath[255] = {0};
    char    *buf;
    int     len;
    #if APL && __MACH__
        char configPath2[255];
        int Result = 0;
    #endif


    #ifndef PluginTest
        XPLMGetSystemPath(configPath);
    #endif
    strcat_s(configPath, 255, "PoKeys.txt");

    #if APL && __MACH__
        Result = ConvertPath(configPath, configPath2, sizeof(configPath));
    if (Result == 0)
        strcpy(configPath, configPath2);
    else
        XPLMDebugString("PoKeys plugin - Unable to convert path\n");
    #endif

    printf("Opening file...\n");
    fopen_s(&gConfigFile, configPath, "r");

    if (gConfigFile == NULL)
    {
        XPLMDebugString("PoKeys plugin - Unable to open configuration file\n");
        return 0;
    } else
    {
        XPLMDebugString("Configuration file opened...\n");

        buf = ReadFileContents(gConfigFile);
        len = (int)strlen(buf);


        XPLMDebugString("Counting lines...\n");
        // Count number of lines...
        int lineNr = 1;
        for (int n = 0; n < len - 1; n++)
        {
            if ((buf[n] == '\r' && buf[n+1] != '\n') || (buf[n] == '\n')) lineNr++;
        }


        XPLMDebugString("Correcting ,,...\n");
        // Count number of double commas
        int doubleCommas = 0;
        for (int n = 0; n < len - 1; n++)
        {
            if (buf[n] == ',' && buf[n+1] == ',') doubleCommas++;
        }

        // Increase buffer to accomodate new spaces to be inserted between double commas
        len += doubleCommas;
        char *buf2 = (char*)malloc(len+1);


        char *src = buf;
        for (int n = 0; n < len - 1; n++)
        {
            if (*src == ',' && *(src+1) == ',')
            {
                // Transfer current character
                buf2[n++] = *src++;
                buf2[n] = ' ';
            } else buf2[n] = *src++;
        }

        buf2[len] = 0;

        // Copy to old buffer
        free(buf);
        buf = buf2;

        printf("Thre are %d lines in the settings file\n", lineNr);

        // Save each line separately...
        char * line;
        char ** lines = (char **)malloc(sizeof(char *) * lineNr);
        memset(lines, 0, sizeof(char *) * lineNr);


        int lineInd = 0;

        char *next_token;
        for (line = strtok_s(buf,"\r\n", &next_token); line != NULL; line = strtok_s(NULL, "\r\n", &next_token))
        {
            if (lineInd < lineNr)
                lines[lineInd++] = line;
        }

        // Print the contents
        if (0)
        {
            printf("Finished reading lines...\n");
            for (int i = 0; i < lineNr; i++)
            {
                if (lines[i] != NULL)
                {
                    printf("Line %d: %s\n", i+1, lines[i]);
                } else
                {
                    printf("Line %d: EMPTY LINE\n", i+1);
                }
            }
        }



        XPLMDebugString("Reading configuration...");

        int deviceIndex = -1;
        int enableNetwork = 0;
        int toggle = 0;

        PK_EnumerateUSBDevices();
        cPKDeviceInterface * dev;

        char debugString[200] = {0};

        for (int i = 0; i < lineNr; i++)
        {
            line = lines[i];
            if (line == NULL) continue;

            //XPLMDebugString("# ");
            //printf("%s\n", line);

            // Check the first character...
            if (line[0] == '[')
            {
                // New device start...
                deviceIndex++;

                // Read serial number...
                long serial = atoi(line+1);

                enableNetwork = 1;
                sprintf(debugString, "[%d] New device configuration (%d / net=%d): ", i, serial, enableNetwork);
                XPLMDebugString(debugString);
                //XPLMDebugString("New device configuration: ");


                //printf(" connecting...");
                sPoKeysDevice * newDev = PK_ConnectToDeviceWSerial_UDP(serial, enableNetwork);
                //printf(" | ");
                devices[deviceIndex].device = newDev;
                //printf(" testing...");
                if (devices[deviceIndex].device == NULL)
                {
                    XPLMDebugString(" Device NOT FOUND!\n");
                    //devices[deviceIndex].device = new sPoKeysDevice();
                } else
                {
                    XPLMDebugString(" Device CONNECTED\n");
                    devices[deviceIndex].InitializeConfig();

                }
                dev = &(devices[deviceIndex]);
            }
            else if (line[0] == '_')
            {
                if (strcmp(line+1, "POKEYS56E") == 0)
                {
                    enableNetwork = 1000;
                    XPLMDebugString("Network devices enabled!\n");
                }
            }
            else if (line[0] == '#')
            {
                // Comment start, skip line
                //XPLMDebugString(" - comment\n");
                continue;
            }
            else
            {
                if (deviceIndex < 0)
                {
                    XPLMDebugString("No device definition before settings!\n");
                    exit(0);
                }

                if (dev->device == NULL)
                {
                    sprintf(debugString, "<%s> Device not present, skipping...\n", line);
                    XPLMDebugString(debugString);
                    continue;
                }

                //XPLMDebugString(" - normal line: ");
                // Check the line length
                if (strlen(line) > 0)
                {
                    // Split the line by commas
                    char * lineParts;

                    int peripheralType = -1;
                    int peripheralIndex = 0;

                    // Get the peripheral type

                    if (strncmp(line, "Pin", 3) == 0)
                    {
                        //XPLMDebugString(" digital pin");
                        peripheralType = 1;
                        peripheralIndex = atoi(line + 3) - 1;
                        //printf(" %d", peripheralIndex);
                    } else if (strncmp(line, "Encoder", 7) == 0)
                    {
                        //XPLMDebugString(" encoder");
                        peripheralType = 2;
                        peripheralIndex = atoi(line + 7) - 1;
                        //printf(" %d", peripheralIndex);
                    } else if (strncmp(line, "LCD", 3) == 0)
                    {
                        //XPLMDebugString(" LCD");
                        if (line[3] == '=')
                        {
                            peripheralType = 3;
                            printf(" LCD configuration");
                        } else
                        {
                            peripheralType = 4;
                        }
                        peripheralIndex = atoi(line + 3) - 1;
                        //printf(" %d", peripheralIndex);
                    } else if (strncmp(line, "PWM", 3) == 0)
                    {
                        if (line[3] == '=')
                        {
                            peripheralType = 5;
                            printf(" PWM configuration");
                        }
                    } else if (strncmp(line, "Ext", 3) == 0)
                    {
                        //XPLMDebugString(" PoExtBus");
                        peripheralType = 10;
                        peripheralIndex = atoi(line + 3) - 1;
                        //printf(" %d", peripheralIndex);
                    } else if (strncmp(line, "MatrixLED1_Digit", 16) == 0)
                    {
                        peripheralType = 30;
                        peripheralIndex = atoi(line + 16) - 1;
                    } else if (strncmp(line, "MatrixLED2_Digit", 16) == 0)
                    {
                        peripheralType = 31;
                        peripheralIndex = atoi(line + 16) - 1;
                    } else if (strncmp(line, "MatrixLED", 9) == 0) // Matrix LED configuration line
                    {
                        if (line[9] == '=')
                        {
                            peripheralType = 35;
                        }
                    } else if (strncmp(line, "PoIL", 4) == 0)
                    {
                        peripheralType = 40;
                        peripheralIndex = atoi(line + 4);
                    }

                    if (peripheralType < 0)
                    {
                        sprintf(debugString, "[%d] unknown peripheral, skipping\n", i);
                        XPLMDebugString(debugString);
                        continue;
                    }
                    //XPLMDebugString("\n");

                    cMapping *k;

                    // Split the line into parts...
                    // Count the number of commas...
                    int partsNr = 1;
                    for (unsigned int j = 0; j < strlen(line); j++)
                    {
                        if (line[j] == ',') partsNr++;
                    }
                    char ** parts = (char**)malloc(sizeof(char *) * partsNr);

                    int part = 0;
                    lineParts = strtok_s(line, "=", &next_token);
                    for (lineParts = strtok_s(NULL, ",", &next_token); lineParts != NULL; lineParts = strtok_s(NULL, ",", &next_token))
                    {
                        if (part < partsNr)
                        {
                            parts[part++] = lineParts;
                        }
                    }

                    if (partsNr > 0)
                    {
                        bool PWMpin = false;
                        switch (peripheralType)
                        {
                            case 1: // Digital pins
                                // Pin direction
                                toggle = 0;
                                if (strcmp(parts[0], "OUT") == 0)
                                {
                                    sprintf(debugString, "[%d] Output pin ", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_digitalOutput;
                                } else if (strcmp(parts[0], "IN") == 0)
                                {
                                    sprintf(debugString, "[%d] Input pin ", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_digitalInput;
                                } else if (strcmp(parts[0], "IN_1") == 0)
                                {
                                    sprintf(debugString, "[%d] Toggled digital input pin - down ", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_digitalInput;
                                    toggle = 1;
                                } else if (strcmp(parts[0], "IN_0") == 0)
                                {
                                    sprintf(debugString, "[%d] Toggled digital input pin - up ", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_digitalInput;
                                    toggle = 2;
                                } else if (strcmp(parts[0], "PWM") == 0)
                                {
                                    PWMpin = true;
                                    if (peripheralIndex >= 16 && peripheralIndex <= 21)
                                    {
                                        sprintf(debugString, "[%d] PWM output ", i);
                                        XPLMDebugString(debugString);
                                        dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_digitalOutput;
                                        dev->device->PWM.PWMenabledChannels[21 - peripheralIndex] = 1;
                                    } else
                                    {
                                        sprintf(debugString, "[%d] PWM pin out of range! ", i);
                                        XPLMDebugString(debugString);
                                        continue;
                                    }

                                } else if (strcmp(parts[0], "AN_IN") == 0)
                                {
                                    sprintf(debugString, "[%d] Analog input ", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction = PK_PinCap_analogInput;
                                }
                                printf(" %d", peripheralIndex);

                                // Check for number of parts...
                                if (partsNr <= 6)
                                {
                                    sprintf(debugString, "[%d] = not enough parameters\n", i);
                                    XPLMDebugString(debugString);
                                    break;
                                }

                                // Inverted status
                                if (strcmp(parts[1], "0") == 0)
                                {
                                    sprintf(debugString, "\n[%d] - inverted", i);
                                    XPLMDebugString(debugString);
                                    dev->device->Pins[peripheralIndex].PinFunction |= PK_PinCap_invertPin;
                                }

                                // Key mapping
                                //printf("\n[%d] - mapped to key %s", i, parts[2]);

                                // Variable name
                                //printf("\n[%d] - mapped to variable %s", i, parts[3]);

                                // Check variable...
                                if (strcmp("pokeys/showstatus", parts[3]) == 0)
                                    bStatusShowVariableMapped = true;

                                XPLMDebugString("\n - Setting-up mapping...");
                                if (PWMpin)
                                {
                                    k = new cMapping(5, 21 - peripheralIndex, parts[3], parseVarThreshold(parts[6]));
                                    k->refType = parseVarType(parts[4]);
                                    k->iIndex = parseVarIndex(parts[5]);

                                    dev->mapList.push_back(k);
                                } else
                                {
                                    if (dev->device->Pins[peripheralIndex].PinFunction & PK_PinCap_digitalInput)
                                    {
                                        if (toggle == 0)
                                        {
                                            k = new cMapping(0, peripheralIndex, parts[3], parseVarThreshold(parts[6]));
                                        } else
                                        {
                                            k = new cMapping(20 + toggle, peripheralIndex, parts[3], parseVarThreshold(parts[6]));
                                        }

                                        k->refType = parseVarType(parts[4]);
                                        k->iIndex = parseVarIndex(parts[5]);

                                        dev->mapList.push_back(k);
                                    } else if (dev->device->Pins[peripheralIndex].PinFunction & PK_PinCap_digitalOutput)
                                    {
                                        k = new cMapping(1, peripheralIndex, parts[3], parseVarThreshold(parts[6]));

                                        k->refType = parseVarType(parts[4]);
                                        k->iIndex = parseVarIndex(parts[5]);

                                        // Check if there is another parameter that specifies the comparison operation
                                        if (partsNr >= 8)
                                        {
                                            k->iComparisonOperation = parseComparisonOp(parts[7]);
                                        }

                                        dev->mapList.push_back(k);
                                    } else if (dev->device->Pins[peripheralIndex].PinFunction & PK_PinCap_analogInput)
                                    {
                                        k = new cMapping(6, peripheralIndex, parts[3], parseVarThreshold(parts[6]));

                                        k->refType = parseVarType(parts[4]);
                                        k->iIndex = parseVarIndex(parts[5]);

                                        dev->mapList.push_back(k);
                                    }
                                }
                                XPLMDebugString(" done\n");

                                break;

                            case 2: // Encoders
                                // Read configuration...
                                sprintf(debugString, "[%d] Encoder configuration:", i);
                                XPLMDebugString(debugString);

                                // Check for number of parts...
                                if (partsNr < 12)
                                {
                                    XPLMDebugString(" not enough parameters\n");
                                    break;
                                }

                                k = new cMapping(12, peripheralIndex, parts[9], parseVarThreshold(parts[3]));
                                k->refType = parseVarType(parts[10]);
                                k->iIndex = parseVarIndex(parts[11]);

                                k->iPinA = atoi(parts[0]) - 1;
                                k->iPinB = atoi(parts[1]) - 1;
                                k->iPinReset = parseOptionalValueI(parts[2], 0) - 1;

                                k->iFastDiff = (int)parseOptionalValueF(parts[4], -1);
                                k->fFastMult = parseOptionalValueF(parts[5], 1);
                                k->fMinValue = (float)atof(parts[6]);
                                k->fMaxValue = (float)atof(parts[7]);
                                k->fDefaultValue = parseOptionalValueF(parts[8], 0);

                                dev->device->Encoders[peripheralIndex].channelApin = k->iPinA;
                                dev->device->Encoders[peripheralIndex].channelBpin = k->iPinB;
                                dev->device->Encoders[peripheralIndex].encoderOptions = (1<<0) | (1<<1);

                                dev->device->Pins[k->iPinA].PinFunction = PK_PinCap_digitalInput;
                                dev->device->Pins[k->iPinB].PinFunction = PK_PinCap_digitalInput;

                                if (k->iPinReset > 55) k->iPinReset = -1;

                                if (k->iPinReset >= 0)
                                {
                                    dev->device->Pins[k->iPinReset].PinFunction = PK_PinCap_digitalInput;
                                }

                                dev->mapList.push_back(k);

                                sprintf_s(debugString, 200, "[%d] A=%d B=%d reset=%d, step=%f, fast=%d, fastMult=%f\n", peripheralIndex, k->iPinA, k->iPinB, k->iPinReset, k->fThreshold, k->iFastDiff, k->fFastMult);
                                XPLMDebugString(debugString);

                                break;
                            case 3: // LCD configuration
                                dev->device->LCD.Configuration = atoi(parts[0]);
                                dev->device->LCD.Rows = atoi(parts[1]);
                                dev->device->LCD.Columns = atoi(parts[2]);

                                sprintf_s(debugString, 200, "[%d] LCD config: %d, %dx%d\n", i, dev->device->LCD.Configuration, dev->device->LCD.Rows, dev->device->LCD.Columns);
                                XPLMDebugString(debugString);

                                //PK_LCDConfigurationSet(dev->device);

                                /*
                                sprintf((char*)dev->device->LCD.line1, "LCD test");
                                dev->device->LCD.RowRefreshFlags |= (1<<0);

                                PK_LCDUpdate(dev->device);*/
                                break;

                            case 4: // LCD contents
                                dev->LCDlines[0].init((char*)dev->device->LCD.line1, 1, parts[0]);
                                //dev->LCDlines[0].RefreshBuffer();
                                //dev->device->LCD.RowRefreshFlags |= dev->LCDlines[0].myLineFlag;
                                //PK_LCDUpdate(dev->device);

                                sprintf_s(debugString, 200, "[%d] Buffer contents: %s\n", i, dev->device->LCD.line1);
                                XPLMDebugString(debugString);
                                break;

                            case 5: // PWM configuration
                                dev->device->PWM.PWMperiod = atoi(parts[0]);
                                break;

                            case 10: // PoExtBus
                                sprintf(debugString, "[%d] PoExtBus output %d", i, peripheralIndex);
                                XPLMDebugString(debugString);

                                if (partsNr < 4)
                                {
                                    XPLMDebugString(" = not enough parameters\n");
                                    break;
                                }

                               // XPLMDebugString("Parsing entry...\r\n");
                                k = new cMapping(10, peripheralIndex, parts[0], parseVarThreshold(parts[3]));

                                //XPLMDebugString("Additional parameters...\r\n");

                                // Check if there is another parameter that specifies the comparison operation
                                if (partsNr >= 5)
                                {
                                    //XPLMDebugString("Operator specifier...\r\n");
                                    k->iComparisonOperation = parseComparisonOp(parts[4]);

                                    //sprintf(debugString, "[%d] Comparison op=%d", i, k->iComparisonOperation);
                                    //XPLMDebugString(debugString);
                                }

                                k->refType = parseVarType(parts[1]);
                                k->iIndex = parseVarIndex(parts[2]);

                                dev->mapList.push_back(k);
                                //XPLMDebugString("Done...\n");
                                break;

                            // Matrix LED 1
                            case 30:
                                if (partsNr == 1)
                                {
                                    // Constant digit
                                    k = new cMapping(30, peripheralIndex, "", 0);
                                    k->refType = 0;
                                    k->iIndex = 0;
                                    k->iplaces = 0;
                                    switch (parts[0][0])
                                    {
                                        case '0':
                                            k->izeropad = 1;
                                            break;
                                        case '-':
                                            k->izeropad = 10;
                                            break;
                                        case '_':
                                            k->izeropad = 0;
                                            break;
                                    }
                                    dev->mapList.push_back(k);
                                } else if (partsNr >= 6)
                                {
                                    //printf("Matrix LED digit setup \n");
                                    k = new cMapping(30, peripheralIndex, parts[0], parseVarThreshold(parts[3]));
                                    k->refType = parseVarType(parts[1]);
                                    k->iIndex = parseVarIndex(parts[2]);
                                    k->iplaces = atoi(parts[4]);
                                    switch (parts[5][0])
                                    {
                                        case '0':
                                            k->izeropad = 1;
                                            break;
                                        case '-':
                                            k->izeropad = 10;
                                            break;
                                        case '_':
                                            k->izeropad = 0;
                                            break;
                                    }
                                    /*
                                    // Check if there is another parameter that specifies the comparison operation
                                    if (partsNr >= 7)
                                    {
                                        k->iComparisonOperation = parseComparisonOp(parts[6]);
                                    }*/

                                    dev->mapList.push_back(k);
                                }
                                break;

                            // Matrix LED 2
                            case 31:
                                if (partsNr == 1)
                                {
                                    // Constant digit
                                    k = new cMapping(31, peripheralIndex, "", 0);
                                    k->refType = 0;
                                    k->iIndex = 0;
                                    k->iplaces = 0;
                                    switch (parts[0][0])
                                    {
                                        case '0':
                                            k->izeropad = 1;
                                            break;
                                        case '-':
                                            k->izeropad = 10;
                                            break;
                                        case '_':
                                            k->izeropad = 0;
                                            break;
                                    }
                                    dev->mapList.push_back(k);
                                } else if (partsNr == 6)
                                {
                                    //printf("Matrix LED 2 digit setup \n");
                                    k = new cMapping(31, peripheralIndex, parts[0], parseVarThreshold(parts[3]));
                                    k->refType = parseVarType(parts[1]);
                                    k->iIndex = parseVarIndex(parts[2]);
                                    k->iplaces = atoi(parts[4]);
                                    switch (parts[5][0])
                                    {
                                        case '0':
                                            k->izeropad = 1;
                                            break;
                                        case '-':
                                            k->izeropad = 10;
                                            break;
                                        case '_':
                                            k->izeropad = 0;
                                            break;
                                    }

                                    /*
                                    // Check if there is another parameter that specifies the comparison operation
                                    if (partsNr >= 7)
                                    {
                                        k->iComparisonOperation = parseComparisonOp(parts[6]);
                                    }*/

                                    dev->mapList.push_back(k);
                                }
                                break;

                            // Matrix LED configuration
                            case 35:
                                sprintf_s(debugString, 200, "[%d] Matrix LED configuration...\n", i);
                                XPLMDebugString(debugString);

                                dev->device->MatrixLED[0].rows = atoi(parts[0]);
                                dev->device->MatrixLED[0].columns = 8;
                                if (dev->device->MatrixLED[0].rows) dev->device->MatrixLED[0].displayEnabled = 1; else dev->device->MatrixLED[0].displayEnabled = 0;
                                dev->device->MatrixLED[1].rows = atoi(parts[1]);
                                dev->device->MatrixLED[1].columns = 8;
                                if (dev->device->MatrixLED[1].rows) dev->device->MatrixLED[1].displayEnabled = 1; else dev->device->MatrixLED[1].displayEnabled = 0;

                                break;

                            // PoIL shared data
                            case 40:
                                sprintf_s(debugString, 200, "[%d] PoIL configuration...", i);
                                XPLMDebugString(debugString);
                                if (partsNr < 5)
                                {
                                    sprintf(debugString, "not enough parameters\n");
                                    XPLMDebugString(debugString);
                                    break;
                                }
                                k = new cMapping(40, peripheralIndex, parts[0], parseVarThreshold(parts[3]));
                                k->refType = parseVarType(parts[1]);
                                k->iIndex = parseVarIndex(parts[2]);
                                if (strcmp(parts[4], "out") == 0)
                                {
                                    k->direction = 1;
                                } else
                                {
                                    k->direction = 0;
                                }
                                dev->mapList.push_back(k);
                                break;
                        }

                    }

                    free(parts);



                } else
                {
                    //sprintf(debugString, "[%d]  - line is empty!", i);
                    //XPLMDebugString(debugString);
                }
                //XPLMDebugString("\n");
            }

            //XPLMDebugString(" !LineEnd!\n");
        }

        XPLMDebugString("Configuration load complete...\n");
        free(lines);
        free(buf);
        return 1;
    }
}
