X-Plane plugin for PoKeys devices

---- About ----
The plugin enables PoKeys devices to be used as I/O devices for X-Plane 
simulator environment.

---- License ----
This code is released under GNU General Public License (GPL). By us-
ing this library in any way, you agree with the terms described in License.txt

Sources of the PoKeysLib library are used directly and are found in the 'Lib'
folder.

---- Compiling ----
Qt Creator was used for IDE and controlling the compiling process. Fix
the paths to X-Plane SDKs first in the project file (TestPlugin.pro).



GNU GCC compiler is used on Linux and OS X to compile and install the library. 
Use the following command
  make -f Makefile.noqmake install
on Linux and
  make -f Makefile.noqmake.osx install
on OS X. These will also install the library in /usr/lib folder and copy the 
header file to /usr/include. Sudo may be required to gain write access to these
two folders

Dependencies (Windows):
For correctly linking PoKeysLib library, the following libraries must be 
provided to the linker:
-lsetupapi -lWs2_32 -liphlpapi

