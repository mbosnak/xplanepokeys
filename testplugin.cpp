#include <stdio.h>
#include <string.h>
#include "XPLMDisplay.h"
#include "XPLMGraphics.h"
#include "XPLMProcessing.h"
#include "XPLMDataAccess.h"
#include "XPLMUtilities.h"
#include "lib\PoKeysLib.h"
#include "cPluginInit.h"

XPLMWindowID	gWindow = NULL;
int             configurationLoaded = 0;
int             started = 0;
int             statusWindowClicked = 0;
FILE *          gConfigFile;
bool            bStatusShowVariableMapped;

void MyDrawWindowCallback(
                                   XPLMWindowID         inWindowID,
                                   void *               inRefcon);

void MyHandleKeyCallback(
                                   XPLMWindowID         inWindowID,
                                   char                 inKey,
                                   XPLMKeyFlags         inFlags,
                                   char                 inVirtualKey,
                                   void *               inRefcon,
                                   int                  losingFocus);

int MyHandleMouseClickCallback(
                                   XPLMWindowID         inWindowID,
                                   int                  x,
                                   int                  y,
                                   XPLMMouseStatus      inMouse,
                                   void *               inRefcon);

float	MyFlightLoopCallback(
                                   float                inElapsedSinceLastCall,
                                   float                inElapsedTimeSinceLastFlightLoop,
                                   int                  inCounter,
                                   void *               inRefcon);


/*
 * XPluginStart
 *
 * Our start routine registers our window and does any other initialization we
 * must do.
 *
 */

XPLMDataRef	gShowStatusDataRef = NULL;
void	ShowStatusChangedCallback(void * inRefcon);

PLUGIN_API int XPluginStart(
                        char *		outName,
                        char *		outSig,
                        char *		outDesc)
{
    // Plugin description
    strcpy_s(outName, 50, "PoKeysInterface");
    strcpy_s(outSig, 50, "public.xplane.plugins.pokeys");
    strcpy_s(outDesc, 50, "Plugin for interfacing PoKeys boards");


    // Create a status window
    gWindow = XPLMCreateWindow(
                    50, 600, 300, 200,			/* Area of the window. */
                    1,							/* Start visible. */
                    MyDrawWindowCallback,		/* Callbacks */
                    MyHandleKeyCallback,
                    MyHandleMouseClickCallback,
                    NULL);						/* Refcon - not used. */


    bStatusShowVariableMapped = false;
    XPLMShareData("pokeys/showstatus", xplmType_Int, ShowStatusChangedCallback, NULL);
    gShowStatusDataRef = XPLMFindDataRef("pokeys/showstatus");

    // Load configuration
    if (LoadConfiguration() != 0)
    {
        configurationLoaded = 1;
    }

    // Register the callback
    XPLMRegisterFlightLoopCallback(
            MyFlightLoopCallback,	/* Callback */
            (float)0.05,					/* Interval */
            NULL);					/* refcon not used. */

    /* We must return 1 to indicate successful initialization, otherwise we
     * will not be called back again. */

    return 1;
}


void ShowStatusChangedCallback(void * inRefcon)
{
    UNREFERENCED_PARAMETER(inRefcon);
    if (bStatusShowVariableMapped)
    {
        statusWindowClicked = XPLMGetDatai(gShowStatusDataRef) > 0 ? 0 : 1;

        XPLMDebugString("Status show variable changed: ");
        if (statusWindowClicked)
            XPLMDebugString(" OFF\n");
        else
            XPLMDebugString(" ON\n");
    }
}

/*
 * XPluginStop
 *
 * Our cleanup routine deallocates our window.
 *
 */
PLUGIN_API void	XPluginStop(void)
{
    DisconnectAll();
    XPLMDestroyWindow(gWindow);

    XPLMUnshareData("pokeys/showstatus", xplmType_Int, ShowStatusChangedCallback, NULL);

    /* Close the file */
    //fclose(gConfigFile);
}

/*
 * XPluginDisable
 *
 * We do not need to do anything when we are disabled, but we must provide the handler.
 *
 */
PLUGIN_API void XPluginDisable(void)
{
}

/*
 * XPluginEnable.
 *
 * We don't do any enable-specific initialization, but we must return 1 to indicate
 * that we may be enabled at this time.
 *
 */
PLUGIN_API int XPluginEnable(void)
{
    return 1;
}

/*
 * XPluginReceiveMessage
 *
 * We don't have to do anything in our receive message handler, but we must provide one.
 *
 */
PLUGIN_API void XPluginReceiveMessage(
                    XPLMPluginID	inFromWho,
                    long			inMessage,
                    void *			inParam)
{
    UNREFERENCED_PARAMETER(inFromWho);
    UNREFERENCED_PARAMETER(inMessage);
    UNREFERENCED_PARAMETER(inParam);
}

/*
 * MyDrawingWindowCallback
 *
 * This callback does the work of drawing our window once per sim cycle each time
 * it is needed.  It dynamically changes the text depending on the saved mouse
 * status.  Note that we don't have to tell X-Plane to redraw us when our text
 * changes; we are redrawn by the sim continuously.
 *
 */
void MyDrawWindowCallback(
                                   XPLMWindowID         inWindowID,
                                   void *               inRefcon)
{

    UNREFERENCED_PARAMETER(inRefcon);
    if (statusWindowClicked) return;

    try
    {
        DrawStatus(inWindowID);

        if (started == 0)
        {
            StartRefresh();
            started = 1;
        }
    } catch (int)
    {
        XPLMDebugString("Exception in WindowCallback");
    }
}



float	MyFlightLoopCallback(
                                   float                inElapsedSinceLastCall,
                                   float                inElapsedTimeSinceLastFlightLoop,
                                   int                  inCounter,
                                   void *               inRefcon)
{
    UNREFERENCED_PARAMETER(inElapsedSinceLastCall);
    UNREFERENCED_PARAMETER(inElapsedTimeSinceLastFlightLoop);
    UNREFERENCED_PARAMETER(inCounter);
    UNREFERENCED_PARAMETER(inRefcon);

    try
    {
        RefreshOnce();
    }
    catch (int)
    {
        XPLMDebugString("Exception in MyFlightLoopCallback");
    }

    return (float)0.05;
}

/*
 * MyHandleKeyCallback
 *
 * Our key handling callback does nothing in this plugin.  This is ok;
 * we simply don't use keyboard input.
 *
 */
void MyHandleKeyCallback(
                                   XPLMWindowID         inWindowID,
                                   char                 inKey,
                                   XPLMKeyFlags         inFlags,
                                   char                 inVirtualKey,
                                   void *               inRefcon,
                                   int                  losingFocus)
{
    UNREFERENCED_PARAMETER(inWindowID);
    UNREFERENCED_PARAMETER(inKey);
    UNREFERENCED_PARAMETER(inFlags);
    UNREFERENCED_PARAMETER(inVirtualKey);
    UNREFERENCED_PARAMETER(inRefcon);
    UNREFERENCED_PARAMETER(losingFocus);
}

/*
 * MyHandleMouseClickCallback
 *
 * Our mouse click callback toggles the status of our mouse variable
 * as the mouse is clicked.  We then update our text on the next sim
 * cycle.
 *
 */
int MyHandleMouseClickCallback(
                                   XPLMWindowID         inWindowID,
                                   int                  x,
                                   int                  y,
                                   XPLMMouseStatus      inMouse,
                                   void *               inRefcon)
{
    UNREFERENCED_PARAMETER(inWindowID);
    UNREFERENCED_PARAMETER(x);
    UNREFERENCED_PARAMETER(y);
    UNREFERENCED_PARAMETER(inRefcon);

    if ((inMouse == xplm_MouseDown) || (inMouse == xplm_MouseUp))
    {
        statusWindowClicked = 1;
    }

    /* Returning 1 tells X-Plane that we 'accepted' the click; otherwise
     * it would be passed to the next window behind us.  If we accept
     * the click we get mouse moved and mouse up callbacks, if we don't
     * we do not get any more callbacks.  It is worth noting that we
     * will receive mouse moved and mouse up even if the mouse is dragged
     * out of our window's box as long as the click started in our window's
     * box. */
    return 1;
}
