#ifndef CPKREFRESHTHREAD_H
#define CPKREFRESHTHREAD_H

//#include <QThread>
#include "PoKeysLib.h"
#include <vector>
#include "cmapping.h"

class cPKRefreshThread
{
public:
    cPKRefreshThread();

    void run();
    int CopyValue(char * dst, char * src);
    char GetBoolVariable(cMapping * mapping);

    sPoKeysDevice * device;

    std::vector<cMapping *> inputToVariable;
    std::vector<cMapping *> variableToOutput;
    std::vector<cMapping *> variableToPoExtBus;
    std::vector<cMapping *> variableToMatrixLED;

    int iRunning;
};

#endif // CPKREFRESHTHREAD_H
