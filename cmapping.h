#ifndef CMAPPING_H
#define CMAPPING_H

#include "XPLMDataAccess.h"
#include "XPLMDefs.h"

int parseVarType(char * linePart);
int parseVarIndex(char * linePart);
float parseVarThreshold(char * linePart);
int parseComparisonOp(char * linePart);
float parseOptionalValueF(char * linePart, float defaultValue);
int parseOptionalValueI(char * linePart, int defaultValue);

class cMapping
{
public:
    cMapping(int type, int peripheralIndex, char * variable, float threshold);
    ~cMapping();

    float GetFloatValue();
    char GetBoolValue();

    void SetValue(float value);


    void DefineNumberFormat(int places, int decimals, int zeropad);
    void DefineBufferOffset(int offset);
    void ProduceString(char *dest);
    void CheckDataRef();

    bool doComparisonF(float value);
    //bool doComparisonI(float value1, float value2);

    int iMappingType;

    bool    bForceDataRefUpdate;

    int     iPoKeysPeripheral;
    char *  cVariableName;
    float   fThreshold;
    int     iIndex;

    int     iComparisonOperation; // 0 - GT, 1 - EQ, 2 - LT, 10 - GTE, 11 - NEQ, 12 - LTE

    int     iplaces, idecimals, izeropad;
    int     iBufferOffset;

    int     iPreviousValue;
    bool    bPreviousValueValid;
    int     iPinA;
    int     iPinB;
    int     iPinReset;
    int     iFastDiff;
    float   fFastMult;
    float   fDefaultValue;
    float   fMinValue;
    float   fMaxValue;

    XPLMDataRef dataRef;
    int     refType;

    int     direction;
};


#endif // CMAPPING_H
