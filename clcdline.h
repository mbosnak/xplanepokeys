#ifndef CLCDLINE_H
#define CLCDLINE_H

#include "lib/PoKeysLib.h"
#include <vector>
#include "cmapping.h"
#include <string>

class cLCDline
{
public:
    cLCDline();
    void init(char * dest, char flag, char * contents);

    void RefreshBuffer();

    char myLineFlag;

    char * bufferPtr;

    std::vector<cMapping *> mapList;
};

#endif // CLCDLINE_H
