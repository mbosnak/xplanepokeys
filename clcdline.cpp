#include "clcdline.h"
#include "PluginSetup.h"

cLCDline::cLCDline()
{
    bufferPtr = NULL;
}

void cLCDline::init(char * dest, char flag, char * contents)
{
    // Parse LCD contents and extract variables...
    mapList.clear();

    bufferPtr = dest;
    myLineFlag = flag;

    char *staticCont = dest;

    printf("\nParsing LCD: %s", contents);

    int len = strlen(contents);

    for (int i = 0; i < len; i++)
    {
        // Look for { and }
        if (contents[i] != '{')
        {
            *staticCont++ = contents[i];
        } else
        {
            unsigned int j = 0;
            // Look for the end...
            for (j = i + 1; j < strlen(contents); j++)
            {
                if (contents[j] == '}') break;
            }
            if (j < strlen(contents))
            {
                // The mapping descriptor starts at i+1 and ends at j-1

                // null-terminate the string
                contents[j] = 0;
                i++;

                // Split the contents using the comma
                int partsNr = 1;
                for (unsigned int n = 0; n < strlen(contents + i); n++)
                {
                    if (contents[i+n] == '|') partsNr++;
                }
                char ** parts = (char**)malloc(sizeof(char *) * partsNr);
                char *lineParts, *next_token;

                int part = 0;
                for (lineParts = strtok_s(contents + i, "|", &next_token); lineParts != NULL; lineParts = strtok_s(NULL, "|", &next_token))
                {
                    if (part < partsNr)
                    {
                        parts[part++] = lineParts;
                    }
                }

                if (partsNr == 5)
                {
                    // First part specifies the number format...
                    // Count the number of # to indicate non-zero padding
                    int hash;
                    for (hash = 0; parts[0][hash] == '#'; hash++);

                    // Count the number of 0 to indicate integer places
                    int places;
                    for (places = hash; parts[0][places] == '0'; places++);

                    int decimals = 0;
                    if (places == strlen(parts[0]))
                    {
                        // That's it

                    } else
                    {
                        // Expect .
                        if (parts[0][places] != '.')
                        {
                            printf("\nMissing period character in LCD mapping descriptor");
                            break;
                        }

                        for (decimals = places + 1; parts[0][decimals] == '0'; decimals++);
                        decimals -= places + 1;

                    }

                    printf(" 0%d %d.%d ", hash, places, decimals);

                    // Create new mapping
                    cMapping *m = new cMapping(50, 0, parts[1], parseVarThreshold(parts[4]));
                    m->DefineNumberFormat(places, decimals, places - hash);
                    m->DefineBufferOffset(i - 1);
                    m->refType = parseVarType(parts[2]);
                    m->iIndex = parseVarIndex(parts[3]);

                    mapList.push_back(m);

                    // Pad the buffer...
                    if (decimals > 0)
                    {
                        for (int n = 0; n < places + decimals + 1; n++) *staticCont++ = '_';
                    } else
                    {
                        for (int n = 0; n < places; n++) *staticCont++ = '_';
                    }
                }

                free(parts);
                i = j;
            } else
            {
                printf("\nProblem while parsing LCD mapping - end of variable not found");
            }
        }
    }
}

void cLCDline::RefreshBuffer()
{
    if (bufferPtr == NULL) return;

    for (unsigned int i = 0; i < mapList.size(); i++)
    {
        cMapping * k = (cMapping *)mapList[i];

        k->ProduceString(bufferPtr);
    }
}

