

QT       -= core
QT       -= gui

TARGET = TestPlugin
TEMPLATE = lib

CONFIG += warn_on plugin release
#CONFIG -= thread exceptions qt rtti debug
#CONFIG -= exceptions rtti debug
#CONFIG += debug

VERSION = 1.1.0

INCLUDEPATH += C:/XPSDK213/SDK/CHeaders/XPLM
INCLUDEPATH += C:/XPSDK213/SDK/CHeaders/Wrappers
INCLUDEPATH += C:/XPSDK213/SDK/CHeaders/Widgets

DEFINES += XPLM213

win32 {
    DEFINES += APL=0 IBM=1 LIN=0
    LIBS += -LC:/XPSDK213/SDK/Libraries/Win
    LIBS += -lsetupapi -lWs2_32 -lIphlpapi


    contains(QMAKE_HOST.arch, x86_64):{
        LIBS += -lXPLM_64 -lXPWidgets_64
        QMAKE_LFLAGS *= /MACHINE:X64
        message('This compiles 64-bit lib')
        TARGET = ../../PoKeysPlugin/64/win.xpl
    } else {
        LIBS += -lXPLM -lXPWidgets
        QMAKE_LFLAGS *= /MACHINE:X86
        message('This compiles 32-bit lib')
        TARGET = ../../PoKeysPlugin/32/win.xpl
    }
}

unix:!macx {
        DEFINES += APL=0 IBM=0 LIN=1
        TARGET = lin.xpl
        # WARNING! This requires the latest version of the X-SDK !!!!
        QMAKE_CXXFLAGS += -fvisibility=hidden
}

macx {
        DEFINES += APL=1 IBM=0 LIN=0
        TARGET = mac.xpl
        QMAKE_LFLAGS += -flat_namespace -undefined suppress

        # Build for multiple architectures.
        # The following line is only needed to build universal on PPC architectures.
        # QMAKE_MAC_SDK=/Developer/SDKs/MacOSX10.4u.sdk
        # The following line defines for which architectures we build.
        CONFIG += x86 ppc
}


SOURCES += testplugin.cpp \
    cpkdeviceinterface.cpp \
    cmapping.cpp \
    cPluginInit.cpp \
    clcdline.cpp \
    lib/PoKeysLibSPI.c \
    lib/PoKeysLibRTC.c \
    lib/PoKeysLibPulseEngine_v2.c \
    lib/PoKeysLibPoNET.c \
    lib/PoKeysLibPoIL.c \
    lib/PoKeysLibMatrixLED.c \
    lib/PoKeysLibMatrixKB.c \
    lib/PoKeysLibLCD.c \
    lib/PoKeysLibIO.c \
    lib/PoKeysLibI2C.c \
    lib/PoKeysLibEncoders.c \
    lib/PoKeysLibDeviceData.c \
    lib/PoKeysLibCoreSockets.c \
    lib/PoKeysLibCore.c \
    lib/hid.c \
    lib/PoKeysLib1Wire.c \
    lib/PoKeysLibFastUSB.c \
    lib/PoKeysLibCAN.c \
    lib/PoKeysLibEasySensors.c \
    lib/PoKeysLibFailsafe.c \
    lib/PoKeysLibUART.c \
    lib/PoKeysLibWS2812.c

HEADERS += testplugin.h\
        TestPlugin_global.h \
    PoKeysLib.h \
    cPluginInit.h \
    cpkdeviceinterface.h \
    cmapping.h \
    PluginSetup.h \
    lib/PoKeysLibCoreSockets.h \
    lib/PoKeysLibCore.h \
    lib/PoKeysLib_global.h \
    lib/PoKeysLib.h \
    lib/hidapi.h \
    lib/hidapi.h \
    lib/PoKeysLibCoreSockets.h

