#include "cpkdeviceinterface.h"
#include <stdio.h>
#include <string.h>
#include "XPLMUtilities.h"
#include "PluginSetup.h"

cPKDeviceInterface::cPKDeviceInterface()
{
    device = NULL;

    mapList.clear();
}

cPKDeviceInterface::~cPKDeviceInterface()
{
    // If device exists, disconnect the device then delete the objects
    if (device != NULL)
    {
        PK_DisconnectDevice(device);
        delete device;
    }

    mapList.clear();
}

// Read the default configuration from the device
void cPKDeviceInterface::InitializeConfig()
{
    if (device == NULL) return;

    PK_DeviceDataGet(device);
    PK_PinConfigurationGet(device);
    PK_EncoderConfigurationGet(device);
    PK_LCDConfigurationGet(device);
    PK_MatrixLEDConfigurationGet(device);
    PK_LCDConfigurationGet(device);
    PK_PWMConfigurationGet(device);
    PK_PoExtBusGet(device);
}

void cPKDeviceInterface::UpdateConfig()
{
    if (device == NULL) return;

    PK_PinConfigurationSet(device);
    PK_EncoderConfigurationSet(device);
    PK_LCDConfigurationSet(device);
    PK_MatrixLEDConfigurationSet(device);
    PK_LCDConfigurationSet(device);
    PK_PWMConfigurationSet(device);
}

void cPKDeviceInterface::PrintMappings(void)
{
    printf("\n -- Variable to output: \n");
    for (unsigned int i = 0; i < mapList.size(); i++)
    {
        cMapping * k = (cMapping *)mapList[i];
        switch (k->iMappingType)
        {
            case 0:
                printf(" --- Pin %d is mapped to variable %s\n", k->iPoKeysPeripheral, k->cVariableName);
                break;

            case 10:
                printf(" --- PoExtBus output %d is mapped to variable %s\n", k->iPoKeysPeripheral, k->cVariableName);
                break;
        }
    }
}

void cPKDeviceInterface::Shutdown()
{
    int module, output;

    // Read all mappings and update inputs and outputs
    for (unsigned int i = 0; i < mapList.size(); i++)
    {
        cMapping * k = (cMapping *)mapList[i];
        k->CheckDataRef();
        if (k->dataRef == NULL) continue;

        char varValue;

        switch (k->iMappingType)
        {
            case 0:
                // Read variable

                varValue = 0;

                // Digital outputs
                if (k->iMappingType == 0)
                {
                    CopyValue((char *)&device->Pins[k->iPoKeysPeripheral].DigitalValueSet, &varValue);
                }
                break;

            case 5:
                // PWM output
                device->PWM.PWMduty[k->iPoKeysPeripheral] = 0;
                break;

            case 6:
                // Analog input
                break;

            case 10:
                module = 9 - k->iPoKeysPeripheral / 8;
                output = k->iPoKeysPeripheral % 8;

                if (module > 9 || module < 0) continue;
                varValue = 0;

                // Digital outputs
                CopyValue((char *)&device->PoExtBusData[module], &varValue);
                break;

            case 30:
                device->MatrixLED[0].data[k->iPoKeysPeripheral] = 0;
                device->MatrixLED[0].RefreshFlag = 1;
                break;
            case 31:
                device->MatrixLED[1].data[k->iPoKeysPeripheral] = 0;
                device->MatrixLED[1].RefreshFlag = 1;
                break;
        }
    }

    PK_DigitalIOSet(device);
    PK_PoExtBusSet(device);
    PK_MatrixLEDUpdate(device);
    PK_PWMUpdate(device);

    for (int i = 0; i < device->LCD.Rows; i++)
    {
        if (LCDlines[i].bufferPtr != NULL)
        {
            memset(LCDlines[i].bufferPtr, ' ', 20);
            device->LCD.RowRefreshFlags |= LCDlines[i].myLineFlag;
        }
    }

    PK_LCDUpdate(device);
}

int getDigit(double digitValue, int digit)
{
    if (digitValue < 0) digitValue *= -1;
    digitValue += 0.005;
    return (int)(digitValue * pow(10.0, -digit)) % 10;
}


void cPKDeviceInterface::RefreshOnce(int forceChange)
{
    int changeIO = forceChange;
    int changePoExt = forceChange;
    int changeLCD = forceChange;
    int changePWM = forceChange;
    char varValue = 0;
    int module, output;
    int digit, digitOpt, minus;
    float digitValue, powerDigit;
    unsigned char digitPrev = 0;
    //char sbuffer[1000] = {0};
    int slotValues[64] = {0};

    //unsigned char LEDdigits[] = {0xED, 0x28, 0xB5, 0xB9, 0x78, 0xD9, 0xDD, 0xA8, 0xFD, 0xF9, 0x10 };
    unsigned char LEDdigits[] = { 0xFC, 0x60, 0xDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0, 0xFE, 0xF6, 0x02 };

    // Read all PoIL shared data slots
    PK_PoILReadSharedSlot(device, 0, 64, slotValues);

    // Read all mappings and update inputs and outputs
    for (unsigned int i = 0; i < mapList.size(); i++)
    {
        cMapping * k = (cMapping *)mapList[i];

        k->CheckDataRef();
        //sprintf_s(sbuffer, 1000, "Refreshing mapping %d, type=%d", i, k->iMappingType);
        //XPLMDebugString(sbuffer);
        if (k->dataRef == NULL)
        {
            //XPLMDebugString(" - dataRef is NULL, skipping\n");
            continue;
        }
        //XPLMDebugString("\n");

        switch (k->iMappingType)
        {
            case 0:
                // Digital inputs
                break;

            case 1:
                // Digital outputs

                // Read variable
                varValue = k->GetBoolValue(); //  GetBoolVariable(k);

                changeIO |= CopyValue((char *)&device->Pins[k->iPoKeysPeripheral].DigitalValueSet, &varValue);
                break;

            case 5:
                // PWM output
                changePWM |= CopyValue32(&device->PWM.PWMduty[k->iPoKeysPeripheral], (uint32_t)(device->PWM.PWMperiod * k->GetFloatValue() / 100.0));
                break;

            case 10:
                // PoExtBus output
                module = 9 - k->iPoKeysPeripheral / 8;
                output = k->iPoKeysPeripheral % 8;

                if (module > 9 || module < 0) continue;


                // Read variable
                varValue = k->GetBoolValue(); //GetBoolVariable(k);

                if (varValue)
                {
                    varValue = device->PoExtBusData[module] | (1<<output);
                } else
                {
                    varValue = device->PoExtBusData[module] & ~(1<<output);
                }

                // Digital outputs
                changePoExt |= CopyValue((char *)&device->PoExtBusData[module], &varValue);

                //sprintf_s(sbuffer, 1000, "PoExtBus %d/%d %d->%d\n", module, output, varValue, changePoExt);
                //XPLMDebugString(sbuffer);

                break;

                // Matrix LED 1
            case 30:
                if (k->cVariableName[0] == 0)
                {
                    digitValue = 0;
                    digit = 1;
                } else
                {
                    digitValue = k->GetFloatValue();
                    digit = k->iplaces;

                }

                digitOpt = k->izeropad;

                if (k->iPoKeysPeripheral < 0 || k->iPoKeysPeripheral > 7) break;

                digitPrev = device->MatrixLED[0].data[k->iPoKeysPeripheral];

                minus = 0;
                if (digitValue < 0)
                {
                    minus = 1;
                    digitValue *= -1;
                }

                if (digit < 0)
                {
                    // digits to the right of the decimal point
                    device->MatrixLED[0].data[k->iPoKeysPeripheral] = LEDdigits[getDigit(digitValue, digit)]; // LEDdigits[((int)(floor(digitValue * pow(10.0, -(int)digit)+0.5)) % 10)];
                } else
                {
                    if (digitOpt == 10 && minus == 1)
                    {
                        // Display - sign
                        device->MatrixLED[0].data[k->iPoKeysPeripheral] = LEDdigits[10];
                    } else
                    {
                        // digits to the left of the decimal point
                        powerDigit = (float)pow(10.0, (int)digit);
                        if (digitValue < powerDigit && digit > 0)
                        {
                            // Display 0 or ' '
                            if (digitOpt == 1) device->MatrixLED[0].data[k->iPoKeysPeripheral] = LEDdigits[0];
                            else device->MatrixLED[0].data[k->iPoKeysPeripheral] = 0;
                        } else
                        {
                            device->MatrixLED[0].data[k->iPoKeysPeripheral] = LEDdigits[((int)((digitValue / powerDigit)) % 10)];
                        }
                    }
                }

                // Display decimal point
                if (digit == 0) device->MatrixLED[0].data[k->iPoKeysPeripheral] |= 1;


                if (digitPrev != device->MatrixLED[0].data[k->iPoKeysPeripheral])
                {
                    device->MatrixLED[0].RefreshFlag = 1;
                }

                break;

                // Matrix LED 2
            case 31:
                if (k->cVariableName[0] == 0)
                {
                    digitValue = 0;
                    digit = 1;
                } else
                {
                    digitValue = k->GetFloatValue();
                    digit = k->iplaces;

                }

                digitOpt = k->izeropad;

                if (k->iPoKeysPeripheral < 0 || k->iPoKeysPeripheral > 7) break;

                digitPrev = device->MatrixLED[1].data[k->iPoKeysPeripheral];

                minus = 0;
                if (digitValue < 0)
                {
                    minus = 1;
                    digitValue *= -1;
                }

                if (digit < 0)
                {
                    // digits to the right of the decimal point
                    device->MatrixLED[1].data[k->iPoKeysPeripheral] = LEDdigits[getDigit(digitValue, digit)]; // = LEDdigits[((int)(floor(digitValue * pow(10.0, -(int)digit)+0.5)) % 10)];
                } else
                {
                    if (digitOpt == 10 && minus == 1)
                    {
                        // Display - sign
                        device->MatrixLED[1].data[k->iPoKeysPeripheral] = LEDdigits[10];
                    } else
                    {
                        // digits to the left of the decimal point
                        powerDigit = (float)pow(10.0, (int)digit);
                        if (digitValue < powerDigit && digit > 0)
                        {
                            // Display 0 or ' '
                            if (digitOpt == 1) device->MatrixLED[1].data[k->iPoKeysPeripheral] = LEDdigits[0];
                            else device->MatrixLED[1].data[k->iPoKeysPeripheral] = 0;
                        } else
                        {
                            device->MatrixLED[1].data[k->iPoKeysPeripheral] = LEDdigits[((int)((digitValue / powerDigit)) % 10)];
                        }
                    }
                }

                // Display decimal point
                if (digit == 0) device->MatrixLED[1].data[k->iPoKeysPeripheral] |= 1;


                if (digitPrev != device->MatrixLED[1].data[k->iPoKeysPeripheral])
                {
                    device->MatrixLED[1].RefreshFlag = 1;
                }

                break;

            case 40:
                if (k->direction == 1)
                {
                    //XPLMDebugString("PoIL out\n");
                    // Write to the slot
                    int val = (int)k->GetFloatValue();

                    // If there is a change, write it back
                    if (val != slotValues[k->iPoKeysPeripheral])
                    {
                        slotValues[k->iPoKeysPeripheral] = val;
                        PK_PoILWriteSharedSlot(device, k->iPoKeysPeripheral, 1, &slotValues[k->iPoKeysPeripheral]);
                    }
                }
                break;
        }
    }

    if (changeIO)
    {
        PK_DigitalIOSetGet(device);
    } else
    {
        PK_DigitalIOGet(device);
    }
    if (changePWM)
    {
        PK_PWMUpdate(device);
    }

    PK_MatrixLEDUpdate(device);
    PK_AnalogIOGet(device);
    PK_EncoderValuesGet(device);

    //XPLMDebugString("Updating the input mappings...\n");

    // Read all mappings and update inputs and outputs
    for (unsigned int i = 0; i < mapList.size(); i++)
    {
        cMapping * k = (cMapping *)mapList[i];
        if (k->dataRef == NULL) continue;

        switch (k->iMappingType)
        {
            case 0:
                k->SetValue(device->Pins[k->iPoKeysPeripheral].DigitalValueGet);
                break;

            case 6:
                // Analog input
                k->SetValue((float)device->Pins[k->iPoKeysPeripheral].AnalogValue / 4095.0f);
                break;

            case 12:
                if (k->bPreviousValueValid)
                {
                    float diff = (float)(device->Encoders[k->iPoKeysPeripheral].encoderValue - k->iPreviousValue) / 4;

                    // Check for fast changes...
                    if (k->iFastDiff > 0)
                    {
                        if (fabs(diff) >= k->iFastDiff)
                        {
                            diff *= k->fFastMult;
                        }
                    }

                    if (fabs(diff) > 0)
                    {
                        float newValue = diff + (k->GetFloatValue() / k->fThreshold / k->fThreshold);
                        if (k->fMaxValue >= k->fMinValue)
                        {
                            if (newValue > k->fMaxValue / k->fThreshold) newValue = k->fMaxValue / k->fThreshold;
                            if (newValue < k->fMinValue / k->fThreshold) newValue = k->fMinValue / k->fThreshold;
                        } else
                        {
                            // minimum and maximum values are reversed - use wrap-around...
                            if (newValue > k->fMinValue / k->fThreshold) newValue = k->fMaxValue / k->fThreshold + (newValue - k->fMinValue / k->fThreshold);
                            if (newValue < k->fMaxValue / k->fThreshold) newValue = k->fMinValue / k->fThreshold + (newValue - k->fMaxValue / k->fThreshold);
                        }
                        /*
                        char sbuffer[200];
                        sprintf_s(sbuffer, 200, "Encoder %d: %d -> %d | Diff=%f New value=%f\n", k->iPoKeysPeripheral, k->iPreviousValue, device->Encoders[k->iPoKeysPeripheral].encoderValue, diff, newValue);
                        XPLMDebugString(sbuffer);
*/

                        k->SetValue(newValue);
                    }
                }
                if (k->iPinReset >= 0)
                {
                    if (device->Pins[k->iPinReset].DigitalValueGet == 0)
                    {
                        k->SetValue(k->fDefaultValue / k->fThreshold);
                    }
                }

                k->iPreviousValue = device->Encoders[k->iPoKeysPeripheral].encoderValue;
                k->bPreviousValueValid = true;
                break;

            case 21:
                if (device->Pins[k->iPoKeysPeripheral].DigitalValueGet)
                {
                    k->SetValue(1);
                }
                break;
            case 22:
                if (device->Pins[k->iPoKeysPeripheral].DigitalValueGet == 0)
                {
                    k->SetValue(1);
                }
                break;

            case 40:
                if (k->direction == 0)
                {
                    //XPLMDebugString("PoIL in\n");

                    //sprintf_s(sbuffer, 1000, "Setting variable %s to %d (slot0=%d slot1=%d)\n", k->cVariableName, slotValues[k->iPoKeysPeripheral], slotValues[0], slotValues[1]);
                    //XPLMDebugString(sbuffer);

                    k->SetValue((float)slotValues[k->iPoKeysPeripheral]);
                }
                break;
        }
    }

    if (changePoExt)
    {
        PK_PoExtBusSet(device);
    }

    // Refresh LCD lines...
    char buffer[20];

    for (int i = 0; i < device->LCD.Rows; i++)
    {
        if (LCDlines[i].bufferPtr != NULL)
        {
            memcpy(buffer, LCDlines[i].bufferPtr, 20);
            LCDlines[i].RefreshBuffer();

            if (memcmp(buffer, LCDlines[i].bufferPtr, 20) != 0 || forceChange)
            {
                device->LCD.RowRefreshFlags |= LCDlines[i].myLineFlag;
                changeLCD = 1;
            }
        }
    }

    if (changeLCD)
    {
        PK_LCDUpdate(device);
    }
}

int cPKDeviceInterface::CopyValue(char *dst, char *src)
{
    if (*dst != *src)
    {
        *dst = *src;
        return 1;
    }
    return 0;
}

int cPKDeviceInterface::CopyValueF(float *dst, float value)
{
    if (*dst != value)
    {
        *dst = value;
        return 1;
    }
    return 0;
}

int cPKDeviceInterface::CopyValue32(uint32_t *dst, uint32_t value)
{
    if (*dst != value)
    {
        *dst = value;
        return 1;
    }
    return 0;
}

