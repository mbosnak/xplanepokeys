#include "cpkrefreshthread.h"
#include "XPLMDataAccess.h"
#include "stdio.h"

cPKRefreshThread::cPKRefreshThread()
{
    iRunning = 1;
}

// Main refresh loop - runs at 20 Hz separately for each device
void cPKRefreshThread::run()
{
    int change = 1;

    // Send settings to device
    PK_PinConfigurationSet(device);



    printf("Starting loop...\n");
    while (iRunning)
    {
        //QThread::msleep(50);

        //printf("Test %d\n", variableToOutput.size());
        // Read all mappings and update inputs and outputs
        for (int i = 0; i < variableToOutput.size(); i++)
        {
            cMapping * k = (cMapping *)variableToOutput[i];

            if (k->dataRef == NULL) continue;

            // Read variable
            char varValue = 0;

            varValue = GetBoolVariable(k);

            // Digital outputs
            if (k->iMappingType == 0)
            {
                change |= CopyValue((char *)&device->Pins[k->iPoKeysPeripheral].DigitalValueSet, &varValue);
            }

            printf("Pin %d = %d\n", k->iPoKeysPeripheral, varValue);
        }
        if (change)
        {
            printf("Pin 1: %d\n", device->Pins[0].DigitalValueSet);
            printf("Pin 10: %d\n", device->Pins[9].DigitalValueSet);

            PK_DigitalIOSet(device);
        }
        change = 0;
    }
}

int cPKRefreshThread::CopyValue(char *dst, char *src)
{
    if (*dst != *src)
    {
        *dst = *src;
        return 1;
    }
    return 0;
}

char cPKRefreshThread::GetBoolVariable(cMapping * mapping)
{
    XPLMDataTypeID id = XPLMGetDataRefTypes(mapping->cVariableName);

    if (id & xplmType_Int)
    {
        // Integer type
        return (XPLMGetDatai(mapping->dataRef) > mapping->fThreshold) ? 1 : 0;
    }
    if (id & xplmType_Float)
    {
        // Float type
        return (XPLMGetDataf(mapping->dataRef) > mapping->fThreshold) ? 1 : 0;
    }
    if (id & xplmType_IntArray)
    {
        // Integer type
        return (XPLMGetDatavi(mapping->dataRef, NULL, mapping->iIndex, mapping->iIndex) > mapping->fThreshold) ? 1 : 0;
    }
    if (id & xplmType_FloatArray)
    {
        // Integer type
        return (XPLMGetDatavf(mapping->dataRef, NULL, mapping->iIndex, mapping->iIndex) > mapping->fThreshold) ? 1 : 0;
    }

    return 0;
}
