#ifndef CPKDEVICEINTERFACE_H
#define CPKDEVICEINTERFACE_H

#include "lib/PoKeysLib.h"
#include <vector>
#include "cmapping.h"
#include <string>
#include "clcdline.h"

class cPKDeviceInterface
{
public:

    cPKDeviceInterface();
    ~cPKDeviceInterface();

    void InitializeConfig(void);
    void UpdateConfig(void);
    void PrintMappings(void);
    void RefreshOnce(int forceChange);
    void Shutdown();

    int CopyValue(char * dst, char * src);
    int CopyValueF(float *dst, float value);
    int CopyValue32(uint32_t *dst, uint32_t value);

    sPoKeysDevice * device;
    cLCDline LCDlines[4];
    std::vector<cMapping *> mapList;
    int iRunning;
};

#endif // CPKDEVICEINTERFACE_H
