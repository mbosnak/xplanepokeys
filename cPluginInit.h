#ifndef CPLUGININIT_H
#define CPLUGININIT_H

#include "PluginSetup.h"

extern bool bStatusShowVariableMapped;

char * ReadFileContents(FILE * configFile);
int LoadConfiguration();
void StartRefresh();
void RefreshOnce();
void DisconnectAll();
void InitAll();

int parseVarType(char * linePart);
int parseVarIndex(char * linePart);
float parseVarThreshold(char * linePart);

#ifndef PluginTest
    void DrawStatus(XPLMWindowID inWindowID);
#endif

#endif // CPLUGININIT_H
